"use strict"

const elements = ["pcarre","gcarre","pcercle","gcercle","prectv","grectv","precth","grecth","ptri","gtri"];
const couleurs = ["bleu","vert","jaune","rouge"];
const tailles = {
  "pcarre": {largeur: 100, hauteur: 100},
  "gcarre": {largeur: 200, hauteur: 200},
  "pcercle": {largeur: 100, hauteur: 100},
  "gcercle": {largeur: 200, hauteur: 200},
  "prectv": {largeur: 50, hauteur: 80},
  "grectv": {largeur: 100, hauteur: 160},
  "precth": {largeur: 100, hauteur: 40},
  "grecth": {largeur: 200, hauteur: 80},
  "ptri": {largeur: 100, hauteur: 60},
  "gtri": {largeur: 200, hauteur: 120},
};

const dessins = {
  "maison": [
    {forme: "gcarre", url: "img/gcarre.png", posX: 290, posY: 300, couleur: "vert"},
    {forme: "gtri", url: "img/gtri.png", posX: 290, posY: 175, couleur: "rouge"}
  ],
  "montagnes": [
    {forme: "gtri", url: "img/gtri.png", posX: 240, posY: 300, couleur: "vert"},
    {forme: "ptri", url: "img/ptri.png", posX: 440, posY: 360, couleur: "bleu"}
  ],
  "cosmos": [
    {forme: "gcercle", url: "img/gcercle.png", posX: 175, posY: 135, couleur: "bleu"},
    {forme: "pcercle", url: "img/pcercle.png", posX: 630, posY: 380, couleur: "jaune"}
  ],
  "pont": [
    {forme: "grectv", url: "img/grectv.png", posX: 198, posY: 260, couleur: "vert"},
    {forme: "grectv", url: "img/grectv.png", posX: 506, posY: 260, couleur: "vert"},
    {forme: "grecth", url: "img/grecth.png", posX: 302, posY: 260, couleur: "bleu"}
  ],
  "tablette": [
    {forme: "prectv", url: "img/prectv.png", posX: 306, posY: 320, couleur: "vert"},
    {forme: "prectv", url: "img/prectv.png", posX: 463, posY: 320, couleur: "vert"},
    {forme: "precth", url: "img/precth.png", posX: 360, posY: 320, couleur: "jaune"}
  ],
  "radio": [
    {forme: "grecth", url: "img/grecth.png", posX: 300, posY: 280, couleur: "vert"},
    {forme: "pcercle", url: "img/pcercle.png", posX: 196, posY: 270, couleur: "rouge"},
    {forme: "pcercle", url: "img/pcercle.png", posX: 503, posY: 270, couleur: "rouge"}
  ],
  "fusee": [
    {forme: "grectv", url: "img/grectv.png", posX: 360, posY: 422, couleur: "rouge"},
    {forme: "grectv", url: "img/grectv.png", posX: 360, posY: 256, couleur: "jaune"},
    {forme: "gtri", url: "img/gtri.png", posX: 310, posY: 130, couleur: "vert"}
  ],
  "guitare": [
    {forme: "gcercle", url: "img/gcercle.png", posX: 290, posY: 375, couleur: "vert"},
    {forme: "pcercle", url: "img/pcercle.png", posX: 338, posY: 272, couleur: "vert"},
    {forme: "prectv", url: "img/prectv.png", posX: 364, posY: 187, couleur: "jaune"},
    {forme: "prectv", url: "img/prectv.png", posX: 364, posY: 103, couleur: "jaune"}
  ],
  "arbre1": [
    {forme: "prectv", url: "img/prectv.png", posX: 380, posY: 400, couleur: "jaune"},
    {forme: "pcercle", url: "img/pcercle.png", posX: 300, posY: 306, couleur: "vert"},
    {forme: "pcercle", url: "img/pcercle.png", posX: 408, posY: 306, couleur: "vert"},
    {forme: "pcercle", url: "img/pcercle.png", posX: 354, posY: 218, couleur: "vert"}
  ],
  "arbre2": [
    {forme: "prectv", url: "img/prectv.png", posX: 370, posY: 402, couleur: "bleu"},
    {forme: "gcercle", url: "img/gcercle.png", posX: 192, posY: 236, couleur: "vert"},
    {forme: "gcercle", url: "img/gcercle.png", posX: 398, posY: 236, couleur: "vert"},
    {forme: "gcercle", url: "img/gcercle.png", posX: 296, posY: 60, couleur: "vert"}
  ],
  "chateau1": [
    {forme: "grectv", url: "img/grectv.png", posX: 200, posY: 320, couleur: "bleu"},
    {forme: "grectv", url: "img/grectv.png", posX: 508, posY: 320, couleur: "bleu"},
    {forme: "grecth", url: "img/grecth.png", posX: 304, posY: 400, couleur: "vert"},
    {forme: "ptri", url: "img/ptri.png", posX: 200, posY: 256, couleur: "rouge"},
    {forme: "ptri", url: "img/ptri.png", posX: 508, posY: 256, couleur: "rouge"}
  ],
  "brouette": [
    {forme: "pcercle", url: "img/pcercle.png", posX: 150, posY: 348, couleur: "jaune"},
    {forme: "grecth", url: "img/grecth.png", posX: 240, posY: 280, couleur: "vert"},
    {forme: "precth", url: "img/precth.png", posX: 446, posY: 280, couleur: "jaune"},
    {forme: "precth", url: "img/precth.png", posX: 552, posY: 280, couleur: "rouge"},
    {forme: "prectv", url: "img/prectv.png", posX: 390, posY: 366, couleur: "vert"}
  ],
  "homme": [
    {forme: "precth", url: "img/precth.png", posX: 360, posY: 518, couleur: "bleu"},
    {forme: "prectv", url: "img/prectv.png", posX: 386, posY: 434, couleur: "vert"},
    {forme: "grectv", url: "img/grectv.png", posX: 360, posY: 270, couleur: "rouge"},
    {forme: "prectv", url: "img/prectv.png", posX: 304, posY: 270, couleur: "bleu"},
    {forme: "prectv", url: "img/prectv.png", posX: 464, posY: 270, couleur: "bleu"},
    {forme: "gcercle", url: "img/gcercle.png", posX: 308, posY: 66, couleur: "jaune"}
  ],
  "chateau2": [
    {forme: "grectv", url: "img/grectv.png", posX: 200, posY: 300, couleur: "bleu"},
    {forme: "grectv", url: "img/grectv.png", posX: 512, posY: 300, couleur: "bleu"},
    {forme: "grecth", url: "img/grecth.png", posX: 306, posY: 380, couleur: "bleu"},
    {forme: "grectv", url: "img/grectv.png", posX: 356, posY: 216, couleur: "vert"},
    {forme: "ptri", url: "img/ptri.png", posX: 200, posY: 236, couleur: "rouge"},
    {forme: "ptri", url: "img/ptri.png", posX: 356, posY: 152, couleur: "rouge"},
    {forme: "ptri", url: "img/ptri.png", posX: 512, posY: 236, couleur: "rouge"}
  ],
  "velo": [
    {forme: "gcercle", url: "img/gcercle.png", posX: 100, posY: 320, couleur: "jaune"},
    {forme: "gcercle", url: "img/gcercle.png", posX: 500, posY: 320, couleur: "jaune"},
    {forme: "pcercle", url: "img/pcercle.png", posX: 328, posY: 396, couleur: "rouge"},
    {forme: "prectv", url: "img/prectv.png", posX: 434, posY: 375, couleur: "vert"},
    {forme: "precth", url: "img/precth.png", posX: 296, posY: 328, couleur: "bleu"},
    {forme: "precth", url: "img/precth.png", posX: 405, posY: 328, couleur: "bleu"},
    {forme: "prectv", url: "img/prectv.png", posX: 242, posY: 240, couleur: "bleu"},
    {forme: "prectv", url: "img/prectv.png", posX: 510, posY: 240, couleur: "bleu"},
    {forme: "precth", url: "img/precth.png", posX: 290, posY: 196, couleur: "vert"},
    {forme: "precth", url: "img/precth.png", posX: 484, posY: 196, couleur: "vert"}
  ],
  "sapin1": [
    {forme: "grectv", url: "img/grectv.png", posX: 430, posY: 422, couleur: "vert"},
    {forme: "gtri", url: "img/gtri.png", posX: 176, posY: 300, couleur: "vert"},
    {forme: "gtri", url: "img/gtri.png", posX: 386, posY: 300, couleur: "vert"},
    {forme: "gtri", url: "img/gtri.png", posX: 594, posY: 300, couleur: "vert"},
    {forme: "gtri", url: "img/gtri.png", posX: 282, posY: 176, couleur: "vert"},
    {forme: "gtri", url: "img/gtri.png", posX: 490, posY: 176, couleur: "vert"},
    {forme: "gtri", url: "img/gtri.png", posX: 386, posY: 52, couleur: "vert"},
    {forme: "pcercle", url: "img/pcercle.png", posX: 122, posY: 424, couleur: "jaune"},
    {forme: "pcercle", url: "img/pcercle.png", posX: 626, posY: 424, couleur: "rouge"},
    {forme: "pcercle", url: "img/pcercle.png", posX: 331, posY: 296, couleur: "bleu"},
    {forme: "pcercle", url: "img/pcercle.png", posX: 612, posY: 130, couleur: "vert"},
    {forme: "pcercle", url: "img/pcercle.png", posX: 330, posY: 46, couleur: "rouge"},
    {forme: "pcarre", url: "img/pcarre.png", posX: 10, posY: 492, couleur: "bleu"},
    {forme: "prectv", url: "img/prectv.png", posX: 16, posY: 408, couleur: "rouge"},
  ],
  "sapin2": [
    {forme: "grectv", url: "img/grectv.png", posX: 350, posY: 436, couleur: "bleu"},
    {forme: "precth", url: "img/precth.png", posX: 540, posY: 556, couleur: "jaune"},
    {forme: "pcarre", url: "img/pcarre.png", posX: 650, posY: 496, couleur: "rouge"},
    {forme: "pcercle", url: "img/pcercle.png", posX: 140, posY: 438, couleur: "jaune"},
    {forme: "pcercle", url: "img/pcercle.png", posX: 540, posY: 438, couleur: "vert"},
    {forme: "pcercle", url: "img/pcercle.png", posX: 84, posY: 248, couleur: "rouge"},
    {forme: "pcercle", url: "img/pcercle.png", posX: 620, posY: 248, couleur: "jaune"},
    {forme: "pcercle", url: "img/pcercle.png", posX: 254, posY: 50, couleur: "jaune"},
    {forme: "pcercle", url: "img/pcercle.png", posX: 446, posY: 50, couleur: "rouge"},
    {forme: "gtri", url: "img/gtri.png", posX: 300, posY: 312, couleur: "vert"},
    {forme: "gtri", url: "img/gtri.png", posX: 96, posY: 312, couleur: "vert"},
    {forme: "gtri", url: "img/gtri.png", posX: 506, posY: 312, couleur: "vert"},
    {forme: "gtri", url: "img/gtri.png", posX: 300, posY: 190, couleur: "vert"},
    {forme: "gtri", url: "img/gtri.png", posX: 300, posY: 68, couleur: "vert"},
    {forme: "gtri", url: "img/gtri.png", posX: 146, posY: 126, couleur: "vert"},
    {forme: "gtri", url: "img/gtri.png", posX: 456, posY: 126, couleur: "vert"},
    {forme: "ptri", url: "img/ptri.png", posX: 198, posY: 250, couleur: "vert"},
    {forme: "ptri", url: "img/ptri.png", posX: 504, posY: 250, couleur: "vert"},
    {forme: "ptri", url: "img/ptri.png", posX: 350, posY: 4, couleur: "vert"},
  ],
}

