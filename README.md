Adaptation du logiciel "Blocs logiques" inclus dans le lanceur d'applications Clicmenu de PragmaTICE.

Assembler des formes géométriques simples, de couleurs et de tailles différentes, selon un modèle.

L'application peut être testée en ligne [ici](https://primtux.fr/applications/blocs-logiques/index.html)
